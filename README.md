# 电动螺丝刀

#### 介绍
自制电动螺丝刀， 充电芯片使用TP4056. 

马达
![输入图片说明](images/1.png)
![输入图片说明](images/2.png)

充电芯片

![输入图片说明](imgsimage.png)

模型 使用 Fusion360 制作

![输入图片说明](images/8bc4b8bf9a8f9d19e283fe154d233ce.png)
![输入图片说明](images/47cc8c28b50ea241640b593cbb63c4c.jpg)
![输入图片说明](images/c8d93f8edbef602cb5e59e7fac2e743.png)
![输入图片说明](images/c97cb95159d6730ab44b01753d54c45.jpg)

按钮电路板 原理图

![输入图片说明](images/12331231.png)
![输入图片说明](images/asdfasdfasdfasdfd.png)
![输入图片说明](images/123412341234.png)